package utfpr.ct.dainf.if62c.pratica;

/**
 * Representa uma matriz de valores {@code double}.
 *
 * @author Wilson Horstmeyer Bogadao <wilson@utfpr.edu.br>
 */
public class Matriz {

    // a matriz representada por esta classe
    private final double[][] mat;

    /**
     * Construtor que aloca a matriz.
     *
     * @param m O número de linhas da matriz.
     * @param n O número de colunas da matriz.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz(int m, int n) throws MatrizInvalidaException {
        if(m <= 0 || n <= 0){
            throw new MatrizInvalidaException(m, n);
        }
        mat = new double[m][n];

    }

    /**
     * Retorna a matriz representada por esta classe.
     *
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz() {
        return mat;
    }

    /**
     * Retorna a matriz transposta.
     *
     * @return A matriz transposta.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz getTransposta() throws MatrizInvalidaException {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }

    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     *
     * @param m A matriz a ser somada
     * @return A soma das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz soma(Matriz m) throws MatrizInvalidaException {
        //throw new UnsupportedOperationException("Soma de matrizes não implementada.");
        if(m.mat.length != this.mat.length || this.mat[0].length != m.mat[0].length){
            throw new SomaMatrizesIncompativeisException(this, m);
        }
        Matriz soma = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                soma.mat[i][j] = mat[i][j] + m.mat[i][j];
            }
        }
        return soma;
    }

    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     *
     * @param m A matriz a ser multiplicada
     * @return O produto das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz prod(Matriz m) throws MatrizInvalidaException {
        //throw new UnsupportedOperationException("Produto de matrizes não implementado.");
//        Matriz produto = new Matriz(mat[0].length, m.mat[0][0].length);
//        for (int i = 0; i < mat.length; i++) {
//            for (int j = 0; j < mat[i].length; j++) {
//                produto.mat[i][j] = mat[i][j] * m.mat[i][j];
//            }
//        }
//        return produto;
        if(m.mat.length != this.mat[0].length || this.mat.length != m.mat[0].length){
            throw new ProdMatrizesIncompativeisException(this, m);
        }
        Matriz produto = new Matriz(mat.length, m.mat[0].length);
        double soma = 0;
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < m.mat[i].length; j++) {
                for (int k = 0; k < m.mat.length; k++) {
                    soma = soma + mat[i][k] * m.mat[k][j];
                }
                produto.mat[i][j] = soma;
                soma = 0;
            }
        }

        return produto;
    }

    /**
     * Retorna uma representação textual da matriz. Este método não deve ser
     * usado com matrizes muito grandes pois não gerencia adequadamente o
     * tamanho do string e poderia provocar um uso excessivo de recursos.
     *
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha : mat) {
            s.append("[ ");
            for (double x : linha) {
                s.append(x).append(" ");
            }
            s.append("]");
        }
        return s.toString();
    }

}
