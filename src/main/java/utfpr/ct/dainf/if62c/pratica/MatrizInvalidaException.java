/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1363417
 */
public class MatrizInvalidaException extends Exception {
    private final int i;
    private final int j;

    public MatrizInvalidaException(int i, int j) {
        super(String.format("Matriz de [" + i + "][" + j + "] não pode ser criada"));
        this.i = i;
        this.j = j;
    }

    public int getNumLinhas() {
        return this.i;  
    }
    
    public int getNumColunas() {
        return this.j;
    }
}
