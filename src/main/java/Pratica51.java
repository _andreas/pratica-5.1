
import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;

/**
 * IF62C Fundamentos de Programação 2 Exemplo de programação em Java.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {

    public static void main(String[] args) throws MatrizInvalidaException {
        Matriz orig = null, matriz2 = null, matriz3 = null, matriz0 = null, matrizNeg = null;
        try{
            orig = new Matriz(3, 2);           
        } catch (MatrizInvalidaException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        try{
            matriz2 = new Matriz(2, 2);          
        } catch (MatrizInvalidaException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        try{
            matriz3 = new Matriz(2, 2);       
        } catch (MatrizInvalidaException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        try{
            matriz0 = new Matriz(0, 0);            
        } catch (MatrizInvalidaException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        try{
            matrizNeg = new Matriz(-2, 3);            
        } catch (MatrizInvalidaException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;

        Matriz transp = null;
        try{
            transp = orig.getTransposta();
            System.out.println("Matriz original: " + orig);
            System.out.println("Matriz transposta: " + transp);
        } catch (MatrizInvalidaException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
                
        double[][] m2 = matriz2.getMatriz();
        m2[0][0] = 0.1;
        m2[0][1] = 0.5;
        m2[1][0] = 1.5;
        m2[1][1] = 1.0;
        
        double[][] m3 = matriz3.getMatriz();
        m3[0][0] = 0.5;
        m3[0][1] = 1.5;
        m3[1][0] = 1.0;
        m3[1][1] = 2.5;

        Matriz soma = null;
        
        System.out.println("Matriz 2: " + matriz2);
        System.out.println("Matriz 3: " + matriz3);
        
        try{
            soma = matriz2.soma(matriz3);
            System.out.println("Soma de Matriz 2 com Matriz 3: " + soma);
        } catch(SomaMatrizesIncompativeisException ex){
            System.out.println(ex.getLocalizedMessage());
        }
        
        Matriz somaEr = null;
        try{
            somaEr = orig.soma(matriz3);
        } catch(SomaMatrizesIncompativeisException ex){
            System.out.println(ex.getLocalizedMessage());
        }       
        
        Matriz produto2E3 = null;
        try{
            produto2E3 = matriz2.prod(matriz3);
            System.out.println("Produto de Matriz 2 com Matriz 3: " + produto2E3);
        } catch(ProdMatrizesIncompativeisException ex){
            System.out.println(ex.getLocalizedMessage()); 
        }
        
        Matriz produtoEr = null;
        try{
            produtoEr = orig.prod(matriz3);
        } catch(ProdMatrizesIncompativeisException ex){
            System.out.println(ex.getLocalizedMessage()); 
        }
    }
}
